require('tools-for-instagram');

const workerMaxSeconds = 300;

(async () => {
    // console.log("Press Any key to continue or Ctrl+C to exit");
    // await keypress();

    console.log("Loading workers".bold.green);

    let surfScraper =  executeWorker({
        workerName: 'surfWorker',
        accountLoginFile: 'botAccount',
        timeout: workerMaxSeconds  // Set to Infinity to avoid Timeout
    });
    
    Promise.all([surfScraper]).then((results) => {
        console.log("All workers done: Result: " + JSON.stringify(results))
    });
    
})();