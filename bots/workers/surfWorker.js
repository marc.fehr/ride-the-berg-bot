require('tools-for-instagram');
const fs = require('fs');
const {threadId} = require('worker_threads')
const axios = require('axios');

(async (accountName, callback) => {

    let intervalID;

    function arraysEqual(a1, a2) {
        // Function to compare arrays which contain post IDs
        return JSON.stringify(a1) === JSON.stringify(a2);
    }

    intervalID = setInterval(async (accountName, callback) => {

        const user = 'lifestylesurfshop';
        const webhookURL = 'https://api.vercel.com/v1/integrations/deploy/prj_IgspmPHqKmeq44S4xr13CSc9CaG7/R9YOP3EYgw';

        /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
        console.log("\n-- 1 LOGIN --\n".bold.underline);
        let ig = await login(loadConfig(accountName));

        /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
        console.log("\n-- 2 Get User Info -- \n".bold.underline);
        
        let info = await getUserInfo(ig, user);
        console.log("User information, username: " + info.username);


        /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
        console.log("\n-- 3: Reading old posts from .json file\n".bold.underline);
        
        var oldPosts = JSON.parse(fs.readFileSync('./output/surf_reports_by_user.json', 'utf8'));
        const oldPostIDs = oldPosts.map(el => el.id);
        // console.log(oldPostIDs);

        /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
        console.log("\n-- 4: Comparing new user posts then saving new ones into JSON file \n".bold.underline);
        
        newPosts = await getUserRecentPosts(ig, user);
        const newPostIDs = newPosts.map(el => el.id);
        // console.log(newPostIDs)

        const comparison = await arraysEqual(oldPostIDs, newPostIDs);
        // console.log("\nThe arrays are equal: " + comparison);

        if (!comparison) {

            /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
            console.log("\n-- 5: Triggering webhook for re-deploy because new posts available\n".bold.underline);
            
            await axios
                .post(webhookURL, {
                    posts: newPosts
                })
                .then(res => {
                    console.log(`Webhook status code: ${res.status}\n`);
                    console.log(`Webhook status text: ${res.statusText}\n`);
                })
                .catch(error => {
                    console.error(error)
                })

        } else {

            /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
            console.log("\n-- 5: No new posts found. Not sending webhook trigger.\n".bold.underline);

        }

        await savePosts(ig, newPosts, "surf_reports_by_user");

        /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
        console.log("\n-- 6: Process done!\n".green.bold.underline);

        // If ONLINE_MODE is enabled, this example will run until we send an exit signal
        // process.exit();

    }, 1000 * 60 * 5);

    // return(callback, {key: "value"});
    
})();