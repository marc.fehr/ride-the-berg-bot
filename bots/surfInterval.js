const SurfScraper = require('./surfreports');

const intervalSeconds = 30;

const {
    setIntervalAsync,
    clearIntervalAsync
  } = require('set-interval-async/dynamic')

function callback (res) {
    console.log(JSON.stringify(res));
}

const timer = setIntervalAsync(
    () => {
        console.log('Starting async interval...');
        SurfScraper((callback), intervalSeconds);
    }, 1000 * 30
)