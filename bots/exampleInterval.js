const tfi = require('tools-for-instagram');
const fs = require('fs');
const axios = require('axios');
const colors = require('colors');

//We create schedules in which our bot will have activity
const intervals = { schedulesOfTheDay: [
    {startHour: 05, startMinute: 00, action: "scrape"},
    {startHour: 05, startMinute: 05, action: "scrape"},
    {startHour: 05, startMinute: 10, action: "scrape"},
    {startHour: 05, startMinute: 15, action: "scrape"},
    {startHour: 05, startMinute: 20, action: "scrape"},
    {startHour: 05, startMinute: 25, action: "scrape"},
    {startHour: 05, startMinute: 30, action: "scrape"},
    {startHour: 05, startMinute: 35, action: "scrape"},
    {startHour: 05, startMinute: 40, action: "scrape"},
    {startHour: 05, startMinute: 45, action: "scrape"},
    {startHour: 05, startMinute: 50, action: "scrape"},
    {startHour: 05, startMinute: 55, action: "scrape"},
    {startHour: 06, startMinute: 00, action: "scrape"},
    {startHour: 06, startMinute: 05, action: "scrape"},
    {startHour: 06, startMinute: 10, action: "scrape"},
    {startHour: 06, startMinute: 15, action: "scrape"},
    {startHour: 06, startMinute: 20, action: "scrape"},
    {startHour: 06, startMinute: 25, action: "scrape"},
    {startHour: 06, startMinute: 30, action: "scrape"},
    {startHour: 06, startMinute: 35, action: "scrape"},
    {startHour: 06, startMinute: 40, action: "scrape"},
    {startHour: 06, startMinute: 45, action: "scrape"},
    {startHour: 06, startMinute: 50, action: "scrape"},
    {startHour: 06, startMinute: 55, action: "scrape"},
    {startHour: 07, startMinute: 00, action: "scrape"},
    {startHour: 07, startMinute: 05, action: "scrape"},
    {startHour: 07, startMinute: 10, action: "scrape"},
    {startHour: 07, startMinute: 15, action: "scrape"},
    {startHour: 07, startMinute: 20, action: "scrape"},
    {startHour: 07, startMinute: 25, action: "scrape"},
    {startHour: 07, startMinute: 30, action: "scrape"},
    {startHour: 07, startMinute: 35, action: "scrape"},
    {startHour: 07, startMinute: 40, action: "scrape"},
    {startHour: 07, startMinute: 45, action: "scrape"},
    {startHour: 07, startMinute: 50, action: "scrape"},
    {startHour: 07, startMinute: 55, action: "scrape"},
    {startHour: 08, startMinute: 00, action: "scrape"},
    {startHour: 08, startMinute: 05, action: "scrape"},
    {startHour: 08, startMinute: 10, action: "scrape"},
    {startHour: 08, startMinute: 15, action: "scrape"},
    {startHour: 08, startMinute: 20, action: "scrape"},
    {startHour: 08, startMinute: 25, action: "scrape"},
    {startHour: 08, startMinute: 30, action: "scrape"},
    {startHour: 08, startMinute: 35, action: "scrape"},
    {startHour: 08, startMinute: 40, action: "scrape"},
    {startHour: 08, startMinute: 45, action: "scrape"},
    {startHour: 08, startMinute: 50, action: "scrape"},
    {startHour: 08, startMinute: 55, action: "scrape"},
    {startHour: 09, startMinute: 00, action: "scrape"},
    {startHour: 09, startMinute: 05, action: "scrape"},
    {startHour: 09, startMinute: 10, action: "scrape"},
    {startHour: 09, startMinute: 15, action: "scrape"},
    {startHour: 09, startMinute: 20, action: "scrape"},
    {startHour: 09, startMinute: 25, action: "scrape"},
    {startHour: 09, startMinute: 30, action: "scrape"},
    {startHour: 09, startMinute: 35, action: "scrape"},
    {startHour: 09, startMinute: 40, action: "scrape"},
    {startHour: 09, startMinute: 45, action: "scrape"},
    {startHour: 09, startMinute: 50, action: "scrape"},
    {startHour: 09, startMinute: 55, action: "scrape"},
    {startHour: 10, startMinute: 00, action: "scrape"},
    {startHour: 10, startMinute: 05, action: "scrape"},
    {startHour: 10, startMinute: 10, action: "scrape"},
    {startHour: 10, startMinute: 15, action: "scrape"},
    {startHour: 10, startMinute: 20, action: "scrape"},
    {startHour: 10, startMinute: 25, action: "scrape"},
    {startHour: 10, startMinute: 30, action: "scrape"},
    {startHour: 10, startMinute: 35, action: "scrape"},
    {startHour: 10, startMinute: 40, action: "scrape"},
    {startHour: 10, startMinute: 45, action: "scrape"},
    {startHour: 10, startMinute: 50, action: "scrape"},
    {startHour: 10, startMinute: 55, action: "scrape"},
    {startHour: 11, startMinute: 00, action: "scrape"},
    {startHour: 11, startMinute: 05, action: "scrape"},
    {startHour: 11, startMinute: 10, action: "scrape"},
    {startHour: 11, startMinute: 15, action: "scrape"},
    {startHour: 11, startMinute: 20, action: "scrape"},
    {startHour: 11, startMinute: 25, action: "scrape"},
    {startHour: 11, startMinute: 30, action: "scrape"},
    {startHour: 11, startMinute: 35, action: "scrape"},
    {startHour: 11, startMinute: 40, action: "scrape"},
    {startHour: 11, startMinute: 45, action: "scrape"},
    {startHour: 11, startMinute: 50, action: "scrape"},
    {startHour: 11, startMinute: 55, action: "scrape"},
    {startHour: 12, startMinute: 00, action: "scrape"},
    {startHour: 12, startMinute: 05, action: "scrape"},
    {startHour: 12, startMinute: 10, action: "scrape"},
    {startHour: 12, startMinute: 15, action: "scrape"},
    {startHour: 12, startMinute: 20, action: "scrape"},
    {startHour: 12, startMinute: 25, action: "scrape"},
    {startHour: 12, startMinute: 30, action: "scrape"},
    {startHour: 12, startMinute: 35, action: "scrape"},
    {startHour: 12, startMinute: 40, action: "scrape"},
    {startHour: 12, startMinute: 45, action: "scrape"},
    {startHour: 12, startMinute: 50, action: "scrape"},
    {startHour: 12, startMinute: 55, action: "scrape"},
    {startHour: 13, startMinute: 00, action: "scrape"},
    {startHour: 13, startMinute: 05, action: "scrape"},
    {startHour: 13, startMinute: 10, action: "scrape"},
    {startHour: 13, startMinute: 15, action: "scrape"},
    {startHour: 13, startMinute: 20, action: "scrape"},
    {startHour: 13, startMinute: 25, action: "scrape"},
    {startHour: 13, startMinute: 30, action: "scrape"},
    {startHour: 13, startMinute: 35, action: "scrape"},
    {startHour: 13, startMinute: 40, action: "scrape"},
    {startHour: 13, startMinute: 45, action: "scrape"},
    {startHour: 13, startMinute: 50, action: "scrape"},
    {startHour: 13, startMinute: 55, action: "scrape"},
    {startHour: 14, startMinute: 00, action: "scrape"},
    {startHour: 14, startMinute: 05, action: "scrape"},
    {startHour: 14, startMinute: 10, action: "scrape"},
    {startHour: 14, startMinute: 15, action: "scrape"},
    {startHour: 14, startMinute: 20, action: "scrape"},
    {startHour: 14, startMinute: 25, action: "scrape"},
    {startHour: 14, startMinute: 30, action: "scrape"},
    {startHour: 14, startMinute: 35, action: "scrape"},
    {startHour: 14, startMinute: 40, action: "scrape"},
    {startHour: 14, startMinute: 45, action: "scrape"},
    {startHour: 14, startMinute: 50, action: "scrape"},
    {startHour: 14, startMinute: 55, action: "scrape"},
    {startHour: 15, startMinute: 00, action: "scrape"},
    {startHour: 15, startMinute: 05, action: "scrape"},
    {startHour: 15, startMinute: 10, action: "scrape"},
    {startHour: 15, startMinute: 15, action: "scrape"},
    {startHour: 15, startMinute: 20, action: "scrape"},
    {startHour: 15, startMinute: 25, action: "scrape"},
    {startHour: 15, startMinute: 30, action: "scrape"},
    {startHour: 15, startMinute: 35, action: "scrape"},
    {startHour: 15, startMinute: 40, action: "scrape"},
    {startHour: 15, startMinute: 45, action: "scrape"},
    {startHour: 15, startMinute: 50, action: "scrape"},
    {startHour: 15, startMinute: 55, action: "scrape"},
    {startHour: 16, startMinute: 00, action: "scrape"},
    {startHour: 16, startMinute: 05, action: "scrape"},
    {startHour: 16, startMinute: 10, action: "scrape"},
    {startHour: 16, startMinute: 15, action: "scrape"},
    {startHour: 16, startMinute: 20, action: "scrape"},
    {startHour: 16, startMinute: 25, action: "scrape"},
    {startHour: 16, startMinute: 30, action: "scrape"},
    {startHour: 16, startMinute: 35, action: "scrape"},
    {startHour: 16, startMinute: 40, action: "scrape"},
    {startHour: 16, startMinute: 45, action: "scrape"},
    {startHour: 16, startMinute: 50, action: "scrape"},
    {startHour: 16, startMinute: 55, action: "scrape"},
    {startHour: 17, startMinute: 00, action: "scrape"},
    {startHour: 17, startMinute: 05, action: "scrape"},
    {startHour: 17, startMinute: 10, action: "scrape"},
    {startHour: 17, startMinute: 15, action: "scrape"},
    {startHour: 17, startMinute: 20, action: "scrape"},
    {startHour: 17, startMinute: 25, action: "scrape"},
    {startHour: 17, startMinute: 30, action: "scrape"},
    {startHour: 17, startMinute: 35, action: "scrape"},
    {startHour: 17, startMinute: 40, action: "scrape"},
    {startHour: 17, startMinute: 45, action: "scrape"},
    {startHour: 17, startMinute: 50, action: "scrape"},
    {startHour: 17, startMinute: 55, action: "scrape"},
]}
  
const user = 'lifestylesurfshop';
const webhookURL = 'https://api.vercel.com/v1/integrations/deploy/prj_IgspmPHqKmeq44S4xr13CSc9CaG7/R9YOP3EYgw';
let newPostIDs;

function arraysEqual(a1, a2) {
    return JSON.stringify(a1) === JSON.stringify(a2);
}

setTimeout(async() => {

    console.log('Surf scraper started.');

    /* Surf scraper script */

    /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
    console.log("\n1 -- LOGIN --\n");
    let ig = await login();


    /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
    console.log("\n2 -- Get User Info -- \n");
    
    let info = await getUserInfo(ig, user);
    console.log("User information, username: " + info.username);

    //We start our interval
    setInterval(async() => {

        //Every time our interval is executed we get the hour and minute
        let currentDate = new Date()
        let getHour = currentDate.getHours();
        let getMinutes = currentDate.getMinutes();

        console.log(`Current interval execution time: ${getHour}:${getMinutes}`)

        intervals.schedulesOfTheDay.forEach(async (time) => {

            if (getHour == time.startHour && getMinutes == time.startMinute && time.action == "scrape") {
                /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
                console.log("\n-- 3: Reading old posts from .json file\n");
                
                var oldPosts = JSON.parse(fs.readFileSync('./output/surf_reports_by_user.json', 'utf8'));
                const oldPostIDs = oldPosts.map(el => el.id);
                // console.log(oldPostIDs);

                /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
                console.log("\n-- 4: Comparing new user posts then saving new ones into JSON file \n");
                
                newPosts = await getUserRecentPosts(ig, user);
                newPostIDs = newPosts.map(el => el.id);
                // console.log(newPostIDs)

                const comparison = await arraysEqual(oldPostIDs, newPostIDs);
                // console.log("\nThe arrays are equal: " + comparison);

                if (!comparison) {

                    /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
                    console.log("\n-- 5: Triggering webhook for re-deploy because new posts available\n");
                    
                    await axios
                        .post(webhookURL, {
                            posts: newPosts
                        })
                        .then(res => {
                            console.log(`Webhook status code: ${res.status}\n`);
                            console.log(`Webhook status text: ${res.statusText}\n`);
                        })
                        .catch(error => {
                            console.error(error)
                        })

                } else {

                    /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
                    console.log("\n-- 5: No new posts found. Not sending webhook trigger.\n");

                }

                await savePosts(ig, newPosts, "surf_reports_by_user");

                /* ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
                console.log("\n-- 6: Process done!\n");

                // sleep(60)

                // If ONLINE_MODE is enabled, this example will run until we send an exit signal
                // process.exit();

            }

            // if(getHour == time.startHour && getMinutes == time.startMinute && time.action == "uploadPhoto"){
            //     This is where we are going to upload photos to instagram
            // }

        });

    }, 60 * 1000);

})

  
// const run = () => {
//     scrapeSurfReports().then(result => {
//         console.log('Surf scraper ended.')
//         done++
//         console.log(`Done ${done} times.`)
//         // console.log(`Now waiting for ${intervalSeconds } seconds`.bold.blue)
//         // setTimeout(() => {
//         //     run()
//         // }, intervalSeconds)
//     });
// };
  
// run();